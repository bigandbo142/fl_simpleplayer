package com.flproj.fl_simpleplayer;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

/**
 * Created by hieunt on 3/18/18.
 */

public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
    }
}
