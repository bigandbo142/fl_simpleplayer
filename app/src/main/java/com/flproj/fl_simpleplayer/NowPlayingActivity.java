package com.flproj.fl_simpleplayer;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.flproj.fl_simpleplayer.data.AppDatabase;
import com.flproj.fl_simpleplayer.helper.BroadcastHelper;
import com.flproj.fl_simpleplayer.helper.DatetimeHelper;
import com.flproj.fl_simpleplayer.helper.FileHelper;
import com.flproj.fl_simpleplayer.helper.PreferencesHelper;
import com.flproj.fl_simpleplayer.media.LocalMediaPlayer;
import com.flproj.fl_simpleplayer.media.MediaPlayer;
import com.flproj.fl_simpleplayer.media.MediaPlayerState;
import com.flproj.fl_simpleplayer.media.ProgressUpdateListener;
import com.flproj.fl_simpleplayer.pojos.Episode;
import com.flproj.fl_simpleplayer.pojos.Playlist;
import com.flproj.fl_simpleplayer.services.SimplePlayerService;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.AudioPickActivity;
import com.vincent.filepicker.filter.entity.AudioFile;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.flproj.fl_simpleplayer.helper.BroadcastHelper.EXTRA_STATE;
import static com.flproj.fl_simpleplayer.services.SimplePlayerService.PARAM_EPISODE_OBJ;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_NEED_RECORDER;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_TAKEN_AUTO_SELECTED;

public class NowPlayingActivity extends AppCompatActivity implements AudioManager.OnAudioFocusChangeListener, MediaPlayer.SimpleMediaPlayerListener, ProgressUpdateListener, SeekBar.OnSeekBarChangeListener {

    private final static int REQ_CODE_PICK_SOUND_FILE = 101;
    private static final float DEGREES_IN_CIRCLE            = 360.0f;
    private static final float MS_IN_RECORD_ROTATION        = 1800.0f;

    private ImageButton btn_add;
    private FloatingActionButton fab_add;
    private ImageButton btn_pause;
    private ImageButton btn_stop;
    private ImageButton btn_play;
    private ImageButton btn_next;
    private ImageButton btn_previous;
    private ImageButton btn_repeat;
    private ImageButton btn_reset;
    private ImageButton btn_volume_up;
    private ImageButton btn_volume_down;
    private ImageButton btn_alarm;
    private TextView tv_time_elapsed;
    private TextView tv_time_left;
    private TextView tv_speed;
    private TextView tv_title;
    private TextView tv_author;
    private TextView tv_next_song_title;
    private SeekBar sb_progress;
    private ImageView channel_art;
    private ImageView img_next_song_art;
    private ConstraintLayout view_info;

    private LocalMediaPlayer localMediaPlayer;
    private int mMediaPlayerState = MediaPlayerState.STATE_IDLE;

    private ProgressUpdateReceiver mProgressUpdateReceiver;
    private UIUpdateReceiver mUiUpdateReceiver;
    private MediaStateReceiver mediaStateReceiver;

    private int mCurrentPlaylist = -1;
    private boolean isRepeat = false;

    private static boolean hasTracks = false;
    private static boolean hasNextTrack = false;
    private static boolean hasPreviousTrack = false;

    private MediaMetadataRetriever metaRetriver;

    DialogProperties properties = new DialogProperties();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing);

        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        getViewReferences();
        registerEvent();
        initView();

        mProgressUpdateReceiver = new ProgressUpdateReceiver(this);
        mUiUpdateReceiver = new UIUpdateReceiver(this);
        mediaStateReceiver = new MediaStateReceiver(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, SimplePlayerService.class);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAllPermissions();
        LocalBroadcastManager.getInstance(this).registerReceiver(mProgressUpdateReceiver,
                new IntentFilter(BroadcastHelper.INTENT_PROGRESS_UPDATE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mUiUpdateReceiver,
                new IntentFilter(BroadcastHelper.INTENT_UI_UPDATE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mediaStateReceiver,
                new IntentFilter(BroadcastHelper.INTENT_STATE_UPDATE));
    }

    private void getViewReferences(){
        btn_add = findViewById(R.id.btn_add);
        btn_pause = findViewById(R.id.btn_pause);
        btn_stop = findViewById(R.id.btn_stop);
        btn_next = findViewById(R.id.btn_next);
        btn_previous = findViewById(R.id.btn_previous);
        btn_play = findViewById(R.id.btn_play);
        sb_progress = findViewById(R.id.sb_progress);
        channel_art = findViewById(R.id.channel_art);
        tv_time_elapsed = findViewById(R.id.tv_time_elapsed);
        tv_time_left = findViewById(R.id.tv_time_left);
        tv_speed = findViewById(R.id.tv_speed);
        tv_title = findViewById(R.id.tv_title);
        tv_author = findViewById(R.id.tv_author);
        tv_next_song_title = findViewById(R.id.tv_next_song_title);
        btn_repeat = findViewById(R.id.btn_set_repeat);
        btn_reset = findViewById(R.id.btn_set_reset);
        btn_volume_up = findViewById(R.id.btn_volume_up);
        btn_volume_down = findViewById(R.id.btn_volume_down);
        btn_alarm = findViewById(R.id.btn_alarm);
        img_next_song_art = findViewById(R.id.img_next_song_art);
        fab_add = findViewById(R.id.fab_add);
        view_info = findViewById(R.id.view_info);
    }

    private void registerEvent(){
        sb_progress.setOnSeekBarChangeListener(this);
        fab_add.setOnClickListener(v -> {
//            openSelectFileDialog();

            openAudioFilePicker();
        });

        btn_pause.setOnClickListener(v -> {
            SimplePlayerService.sendIntent(v.getContext(), SimplePlayerService.ACTION_PAUSE, null);
            mMediaPlayerState = MediaPlayerState.STATE_PAUSED;
        });

        btn_stop.setOnClickListener(v -> {
            SimplePlayerService.sendIntent(v.getContext(), SimplePlayerService.ACTION_STOP, null);
            mMediaPlayerState = MediaPlayerState.STATE_IDLE;
        });

        btn_next.setOnClickListener(v -> {
            if(hasNextTrack) {
                SimplePlayerService.changeEpisodeInPlaylist(v.getContext(), true);
                mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
            }else {
                Toast.makeText(v.getContext(), "No audio found", Toast.LENGTH_LONG).show();
            }
        });
        btn_previous.setOnClickListener(v -> {
            if(hasPreviousTrack) {
                SimplePlayerService.changeEpisodeInPlaylist(v.getContext(), false);
                mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
            }else{
                Toast.makeText(v.getContext(), "No audio found", Toast.LENGTH_LONG).show();
            }
        });

        btn_play.setOnClickListener(v -> {
            if(hasTracks) {
                switch (mMediaPlayerState) {
                    case MediaPlayerState.STATE_PAUSED:
                        SimplePlayerService.sendIntent(v.getContext(), SimplePlayerService.ACTION_RESUME_PLAYBACK, null);
                        mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
                        break;
                    case MediaPlayerState.STATE_IDLE:
                    case MediaPlayerState.STATE_ENDED:
                        SimplePlayerService.sendEventRunPlaylist(this, mCurrentPlaylist);
                        mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
                        break;
                }
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true)
                        .setMessage("Please choose audio file")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton("Choose", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                openAudioFilePicker();
                            }
                        })
                        .create().show();
            }
        });

        tv_speed.setOnClickListener(v -> {
            SpeedPickerDialog newFragment = new SpeedPickerDialog();
            newFragment.setValueChangeListener((picker, oldVal, newVal) -> {

                float speed = Float.valueOf(SpeedPickerDialog.SPEED_VALUES[newVal]);
                tv_speed.setText("" + speed + "x");
                SimplePlayerService.changePlaybackSpeed(v.getContext(), speed);
                PreferencesHelper.getInstance(NowPlayingActivity.this).setLastEpisodeSpeed(speed);

            });
            newFragment.show(getSupportFragmentManager(), "time picker");
        });

        btn_reset.setOnClickListener(v -> SimplePlayerService.replayCurrentEpisode(v.getContext()));
        btn_repeat.setOnClickListener(v -> {
            isRepeat = !isRepeat;
            if(isRepeat){
                btn_repeat.setImageResource(R.drawable.ic_repeat_active);
            }else{
                btn_repeat.setImageResource(R.drawable.ic_repeat);
            }
            SimplePlayerService.toggleRepeatCurrentEpisode(v.getContext());
        });

        btn_volume_up.setOnClickListener(v -> SimplePlayerService.changeVolume(v.getContext(), true));
        btn_volume_down.setOnClickListener(v -> SimplePlayerService.changeVolume(v.getContext(), false));

        btn_alarm.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), AlarmSettingActivity.class);
            startActivityForResult(intent, 100);
        });
    }

    private void initView(){
        properties.selection_mode = DialogConfigs.MULTI_MODE;
        properties.selection_type = DialogConfigs.FILE_AND_DIR_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = FileHelper.supportedAudioFormat;


        int playlistId = PreferencesHelper.getInstance(getApplicationContext())
                .getLastPlaylistId();
        int episodeId = PreferencesHelper.getInstance(getApplicationContext())
                .getLastEpisodeId();

        float episodeSpeed = PreferencesHelper.getInstance(getApplicationContext())
                .getLastEpisodeSpeed();

        if(playlistId != -1 && episodeId != -1){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true)
                    .setMessage("Do you want to resume previous playlist ?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton("Resume", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            showPlayerControl(true);
                            mCurrentPlaylist = playlistId;
                            SimplePlayerService.resumeLastPlaylist(NowPlayingActivity.this);
                            tv_speed.setText("" + episodeSpeed + "x");
                            SimplePlayerService.changePlaybackSpeed(NowPlayingActivity.this, episodeSpeed);
                        }
                    })
                    .create().show();

        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true)
                    .setMessage("Please choose audio file")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton("Choose", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            openAudioFilePicker();
                        }
                    })
                    .create().show();
        }
    }

    private void showPlayerControl(boolean show){
        if(show){
            view_info.setVisibility(View.VISIBLE);
            sb_progress.setVisibility(View.VISIBLE);
            tv_time_left.setVisibility(View.VISIBLE);
            tv_time_elapsed.setVisibility(View.VISIBLE);
        }else{
            view_info.setVisibility(View.INVISIBLE);
            sb_progress.setVisibility(View.INVISIBLE);
            tv_time_left.setVisibility(View.INVISIBLE);
            tv_time_elapsed.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Constant.REQUEST_CODE_PICK_AUDIO:
                if (resultCode == RESULT_OK) {

                    showPlayerControl(true);
                    hasTracks = true;

                    ArrayList<AudioFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_AUDIO);

                    List<String> results = new ArrayList<>();
                    for (AudioFile audioFile : list){
                        results.add(audioFile.getPath());
                    }

//                    List<String> results = FileHelper.getAllAudioFileFromFilesOrDirectory(files);

                    mMediaPlayerState = MediaPlayerState.STATE_IDLE;

                    // Handle when play a playlist
                    Playlist newPlaylist = new Playlist();
                    newPlaylist.setCurrentEpisodeId(-1);

                    Single.fromCallable(() -> AppDatabase.getAppDatabase(getApplicationContext()).playlistDao().insertPlaylist(newPlaylist))
                            .map(playlistId -> {
                                try {

//                                    MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                                    for (String path : results){
//                                        metaRetriver.setDataSource(path);
                                        Episode episode = generateEpisodeFromPath(path);

                                        episode.setPath(path);
                                        episode.setPlaylistId(playlistId.intValue());
                                        AppDatabase.getAppDatabase(getApplicationContext()).episodeDao().insertAll(episode);
                                    }
                                    return playlistId.intValue();
                                }catch (Exception ex){
                                    return -1;
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(res -> {
                                if(res >= 0){
                                    mCurrentPlaylist = res.intValue();
                                    SimplePlayerService.sendEventRunPlaylist(NowPlayingActivity.this, res);
                                    mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
                                }

                            }, throwable -> {
                                throwable.printStackTrace();
                            });
                }
                break;
        }
    }

    private void openAudioFilePicker(){
        Intent intent3 = new Intent(this, SimpleAudioPickerActivity.class);
        intent3.putExtra(IS_NEED_RECORDER, false);
        intent3.putExtra(IS_TAKEN_AUTO_SELECTED, false);
        intent3.putExtra(Constant.MAX_NUMBER, 100);
        intent3.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent3, Constant.REQUEST_CODE_PICK_AUDIO);
    }

    private void openSelectFileDialog(){
        if(isEnoughPermissions()) {

            FilePickerDialog dialog = new FilePickerDialog(NowPlayingActivity.this,properties);
            dialog.setTitle("Select a File");
            dialog.setDialogSelectionListener(new DialogSelectionListener() {
                @Override
                public void onSelectedFilePaths(String[] files) {

                    List<String> results = FileHelper.getAllAudioFileFromFilesOrDirectory(files);

                    mMediaPlayerState = MediaPlayerState.STATE_IDLE;
                    if (localMediaPlayer == null) {
                        localMediaPlayer = new LocalMediaPlayer(NowPlayingActivity.this, NowPlayingActivity.this, NowPlayingActivity.this);
                    }

                    // Handle when play a playlist
                    Playlist newPlaylist = new Playlist();
                    newPlaylist.setCurrentEpisodeId(-1);

                    Single.fromCallable(() -> AppDatabase.getAppDatabase(getApplicationContext()).playlistDao().insertPlaylist(newPlaylist))
                            .map(playlistId -> {
                                try {

//                                    MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                                    for (String path : results){
//                                        metaRetriver.setDataSource(path);
                                        Episode episode = generateEpisodeFromPath(path);

                                        episode.setPath(path);
                                        episode.setPlaylistId(playlistId.intValue());
                                        AppDatabase.getAppDatabase(getApplicationContext()).episodeDao().insertAll(episode);
                                    }
                                    return playlistId.intValue();
                                }catch (Exception ex){
                                    return -1;
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(res -> {
                                if(res >= 0){
                                    mCurrentPlaylist = res.intValue();
                                    SimplePlayerService.sendEventRunPlaylist(NowPlayingActivity.this, res);
                                    mMediaPlayerState = MediaPlayerState.STATE_PLAYING;
                                }

                            }, throwable -> {
                                throwable.printStackTrace();
                            });

                }
            });

            dialog.show();
        }
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception ex){
            ex.printStackTrace();
            return "";
        } finally{
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isEnoughPermissions(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkAllPermissions(){
        if (!isEnoughPermissions()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    1001);
        } else {

        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }

    @Override
    public void onProgressUpdate(long progress, long bufferedProgress, long duration) {

    }

    @Override
    public void onStateChanged(int state) {

    }

    private void updatePlaybackStats(long progress, long duration) {
//        channel_art.setRotation(((progress / MS_IN_RECORD_ROTATION) * DEGREES_IN_CIRCLE)
//                % DEGREES_IN_CIRCLE);
        sb_progress.setProgress((int) progress);
        sb_progress.setMax((int) duration);
        long left = duration - progress;

        if (left == 0 && duration == 0) {
            tv_time_elapsed.setText("--:--");
            tv_time_left.setText("--:--");
        } else {
            tv_time_elapsed.setText(DatetimeHelper.convertSecondsToDuration(progress));
            tv_time_left.setText(getString(R.string.time_left, DatetimeHelper.convertSecondsToDuration(left)));
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            SimplePlayerService.sendIntent(this, SimplePlayerService.ACTION_SEEK_TO,
                    progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private static class ProgressUpdateReceiver extends BroadcastReceiver {

        private final WeakReference<NowPlayingActivity> mActivity;

        public ProgressUpdateReceiver(NowPlayingActivity context) {
            mActivity = new WeakReference<>(context);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            NowPlayingActivity activity = mActivity.get();

            if (activity == null) {
                return;
            }
            long progress = intent.getLongExtra(BroadcastHelper.EXTRA_PROGRESS, 0L);
            long bufferedProgress = intent.getLongExtra(BroadcastHelper.EXTRA_BUFFERED_PROGRESS, 0L);
            long duration = intent.getLongExtra(BroadcastHelper.EXTRA_DURATION, 0L);
            activity.sb_progress.setSecondaryProgress((int) bufferedProgress);
            activity.updatePlaybackStats(progress, duration);
        }
    }

    private static class MediaStateReceiver extends BroadcastReceiver {

        private final WeakReference<NowPlayingActivity> mActivity;

        public MediaStateReceiver(NowPlayingActivity context) {
            mActivity = new WeakReference<>(context);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            NowPlayingActivity activity = mActivity.get();

            if (activity == null) {
                return;
            }

            int state = intent.getIntExtra(EXTRA_STATE, MediaPlayerState.STATE_IDLE);
            if(state == MediaPlayerState.STATE_PLAYING){
                activity.btn_play.setVisibility(View.INVISIBLE);
                activity.btn_pause.setVisibility(View.VISIBLE);
            }else{
                activity.btn_play.setVisibility(View.VISIBLE);
                activity.btn_pause.setVisibility(View.INVISIBLE);
            }

        }
    }

    private static class UIUpdateReceiver extends BroadcastReceiver {

        private final WeakReference<NowPlayingActivity> mActivity;

        public UIUpdateReceiver(NowPlayingActivity context) {
            mActivity = new WeakReference<>(context);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            NowPlayingActivity activity = mActivity.get();

            if (activity == null) {
                return;
            }
            String name = intent.getStringExtra(BroadcastHelper.EXTRA_NAME);
            String path = intent.getStringExtra(BroadcastHelper.EXTRA_PATH);
            String nextPath = intent.getStringExtra(BroadcastHelper.EXTRA_NEXT_PATH);
            String prevPath = intent.getStringExtra(BroadcastHelper.EXTRA_PREVIOUS_PATH);
            float speed = intent.getFloatExtra(BroadcastHelper.EXTRA_SPEED, 1.0f);
            if(name == null){
                activity.tv_title.setText("Unknown");
            }else
                activity.tv_title.setText(name);


            activity.tv_speed.setText("" + speed + "x");

            PreferencesHelper.getInstance(activity).setLastEpisodeSpeed(speed);

            if(path != null) {
                Episode episode = activity.generateEpisodeFromPath(path);
                activity.tv_author.setText(episode.getAuthor());
                Bitmap bitmapArt = activity.getBitmapArtFromPath(path);
                if(bitmapArt != null) {
                    activity.channel_art.setImageBitmap(bitmapArt);
                }else{
                    activity.channel_art.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_default_art));
                }
            }

            if(nextPath != null){
                if(nextPath.isEmpty()){
                    activity.tv_next_song_title.setText("");
                    activity.img_next_song_art.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_default_art));
                    hasNextTrack = false;
                }else{
                    Episode nextEpisode = activity.generateEpisodeFromPath(nextPath);
                    activity.tv_next_song_title.setText(nextEpisode.getName());
                    Bitmap bitmapArt = activity.getBitmapArtFromPath(nextPath);
                    if(bitmapArt != null) {
                        activity.img_next_song_art.setImageBitmap(bitmapArt);
                    }else{
                        activity.img_next_song_art.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_default_art));
                    }
                    hasNextTrack = true;
                }
            }else{
                hasNextTrack = false;
            }

            if(prevPath == null || prevPath.isEmpty()){
                hasPreviousTrack = false;
            }else{
                hasPreviousTrack = true;
            }

        }
    }

    private Episode generateEpisodeFromPath(String path){
        if(metaRetriver == null){
            metaRetriver = new MediaMetadataRetriever();
        }
        Episode episode = new Episode();
        metaRetriver.setDataSource(path);

        try {
            String name = metaRetriver
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            if(name == null || name.isEmpty()){
                episode.setName("Unknown");
            }else{
                episode.setName(name);
            }
            String author = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);

            if(author == null || author.isEmpty()){
                episode.setAuthor("Unknown");
            }else{
                episode.setAuthor(author);
            }
        }catch (Exception ex){
            episode.setName("Unknown");
            episode.setAuthor("Unknown");
        }
        episode.setPath(path);

        return episode;
    }

    private Bitmap getBitmapArtFromPath(String path){
        try {
            if (metaRetriver == null) {
                metaRetriver = new MediaMetadataRetriever();
            }
            metaRetriver.setDataSource(path);

            byte[] art = metaRetriver.getEmbeddedPicture();

            Bitmap songImage = BitmapFactory
                    .decodeByteArray(art, 0, art.length);
            return songImage;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
