package com.flproj.fl_simpleplayer.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hieunt on 3/17/18.
 */

public class FileHelper {

    public static String[] supportedAudioFormat = {
            "mp4",
            "m4a",
            "mp3",
            "ogg",
            "wav"
    };

    public enum SupportedFileFormat
    {
//        _3GP("3gp"),
        MP4("mp4"),
        M4A("m4a"),
//        AAC("aac"),
//        TS("ts"),
//        FLAC("flac"),
        MP3("mp3"),
//        MID("mid"),
//        XMF("xmf"),
//        MXMF("mxmf"),
//        RTTTL("rtttl"),
//        RTX("rtx"),
//        OTA("ota"),
//        IMY("imy"),
        OGG("ogg"),
//        MKV("mkv"),
        WAV("wav");

        private String filesuffix;

        SupportedFileFormat( String filesuffix ) {
            this.filesuffix = filesuffix;
        }

        public String getFilesuffix() {
            return filesuffix;
        }
    }

    public static boolean checkExtension( String fileName ) {
        String ext = getFileExtension(fileName);
        if ( ext == null) return false;
        try {
            if ( SupportedFileFormat.valueOf(ext.toUpperCase()) != null ) {
                return true;
            }
        } catch(IllegalArgumentException e) {
            return false;
        }
        return false;
    }

    public static String getFileExtension( String fileName ) {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i+1);
        } else
            return null;
    }

    public static List<String> getAllSupportedFileInDirectory(String path){
        File directory = new File(path);
        List<String> result = new ArrayList();
        if(directory.exists() && directory.isDirectory()){
            File list[] = directory.listFiles();

            for( int i=0; i< list.length; i++)
            {
                if(checkExtension(list[i].getName())){
                    result.add(list[i].getAbsolutePath());
                }
            }
        }
        return result;
    }

    public static List<String> getAllAudioFileFromFilesOrDirectory(String[] paths){
        List<String> result = new ArrayList<>();
        for (int i = 0; i < paths.length; i++){
            File tmpFile = new File(paths[i]);
            if(tmpFile.exists()){
                if(tmpFile.isDirectory()){
                    result.addAll(getAllSupportedFileInDirectory(tmpFile.getAbsolutePath()));
                }else{
                    if(checkExtension(tmpFile.getName())){
                        result.add(tmpFile.getAbsolutePath());
                    }
                }
            }
        }

        return result;
    }
}
