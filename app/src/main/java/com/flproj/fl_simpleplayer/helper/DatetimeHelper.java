package com.flproj.fl_simpleplayer.helper;

import android.content.Context;
import android.util.Log;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import static org.threeten.bp.temporal.TemporalAdjusters.firstDayOfMonth;
import static org.threeten.bp.temporal.TemporalAdjusters.lastDayOfMonth;
import static org.threeten.bp.temporal.TemporalAdjusters.next;
import static org.threeten.bp.temporal.TemporalAdjusters.previous;

/**
 * Created by hieunt on 3/15/18.
 */

public class DatetimeHelper {

    private static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
    private static final int MINUTES_IN_AN_HOUR = 60;
    private static final int SECONDS_IN_A_MINUTE = 60;

    public static long getTimestamp() {
        return System.currentTimeMillis();
    }

    /**
     * Converts a date object to a ISO Date string
     * @param date Date to convert
     * @return ISO Date string
     */
    public static String dateToString(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(ISO_DATE_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    /**
     * Converts a ISO Date String to a Date Object
     * @param dateText ISO Date String
     * @return Date
     * @throws ParseException
     */
    public static Date stringToDate(String dateText) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(ISO_DATE_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.parse(dateText);
    }

    /**
     * Converts a date to human readable format
     * @param date date to convert
     * @return human readable string
     */
    public static String dateToShortReadableString(Context context, Date date) {
        StringBuilder readable = new StringBuilder(32);
        Locale locale = Locale.getDefault();
        Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        gmtCal.setTime(date);
        Calendar localCal = GregorianCalendar.getInstance(TimeZone.getDefault());
        localCal.setTimeInMillis(gmtCal.getTimeInMillis());

        Calendar now = Calendar.getInstance();

        // if date is today
        if (localCal.get(Calendar.DAY_OF_YEAR) >= now.get(Calendar.DAY_OF_YEAR) &&
                localCal.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
            readable.append("Today");
        }

        // if the date is yesterday
        else if (localCal.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR) - 1) {
            readable.append("Yesterday");
        }

        // TODO, if within the last week, show days ago

        // if week of the month, show the day of the week
        else if (localCal.get(Calendar.WEEK_OF_MONTH) == now.get(Calendar.WEEK_OF_MONTH) &&
                localCal.get(Calendar.MONTH) == now.get(Calendar.MONTH)) {
            readable.append(localCal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, locale));
        }

        // if the same year, show the Month + Day
        else if (localCal.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
            readable.append(localCal.getDisplayName(Calendar.MONTH, Calendar.SHORT, locale))
                    .append(" ")
                    .append(localCal.get(Calendar.DAY_OF_MONTH));
        }

        // else show the date in MM/DD/YYYY
        else {
            readable.append(localCal.get(Calendar.MONTH) + 1)
                    .append("/")
                    .append(localCal.get(Calendar.DAY_OF_MONTH))
                    .append("/")
                    .append(localCal.get(Calendar.YEAR));
        }
        return readable.toString();
    }

    public static String convertSecondsToReadableDuration(long totalMilliseconds) {

        if (totalMilliseconds < 0) {
            return " < 1 min";
        }

        long totalSeconds = totalMilliseconds / 1000;
        long totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE;
        long minutes = totalMinutes % MINUTES_IN_AN_HOUR;
        long hours = totalMinutes / MINUTES_IN_AN_HOUR;
        StringBuilder durationStr = new StringBuilder();

        if (hours > 0) {
            durationStr.append(hours).append(" hr ");
        }

        if (hours < 1 && minutes < 1) {
            durationStr.append(" < 1 min");
        } else {
            durationStr.append(minutes).append(" min");
        }
        return durationStr.toString();
    }

    public static String convertSecondsToDuration(long totalMilliseconds) {

        if (totalMilliseconds < 0) {
            return "00:00";
        }

        if (totalMilliseconds < 3_600_000) {

            return DurationFormatUtils.formatDuration(totalMilliseconds, "mm:ss", true);
        } else {
            return DurationFormatUtils.formatDuration(totalMilliseconds, "H:mm:ss", true);
        }
    }

    public static String formatRFC822Date(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
        return format.format(date);
    }

    public static String convertTo24HoursFromTime(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return new SimpleDateFormat("HH:mm", Locale.US).format(calendar.getTime());
    }

    public static String convertToReadableDateFromTime(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        return new SimpleDateFormat("dd MMM yyyy", Locale.US).format(calendar.getTime());
    }

    public static long getNextFirstTuesday(){

        LocalDate firstTuesday = LocalDate
                .now()
                .with(firstDayOfMonth())
                .with(next(DayOfWeek.TUESDAY));

        if(LocalDate.now().isAfter(firstTuesday)){
            firstTuesday = LocalDate.now()
                    .plusMonths(1)
                    .with(firstDayOfMonth())
                    .with(next(DayOfWeek.TUESDAY));
        }

        System.out.println(firstTuesday);
        return firstTuesday.toEpochDay();
    }

    public static long getNextFirstTuesdayWithTime(int hour, int min){
        LocalDate firstTuesday = LocalDate
                .now()
                .with(firstDayOfMonth())
                .with(next(DayOfWeek.TUESDAY));

        if(LocalDate.now().isAfter(firstTuesday)){
            firstTuesday = LocalDate.now()
                    .plusMonths(1)
                    .with(firstDayOfMonth())
                    .with(next(DayOfWeek.TUESDAY));
        }

        return firstTuesday.atTime(hour, min).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static long getNextThirdTuesday(){
        LocalDate thirdTuesday = LocalDate
                .now()
                .with(lastDayOfMonth())
                .with(previous(DayOfWeek.TUESDAY)).minusDays(7);

        if(LocalDate.now().isAfter(thirdTuesday)){
            thirdTuesday = LocalDate.now()
                    .plusMonths(1)
                    .with(lastDayOfMonth())
                    .with(previous(DayOfWeek.TUESDAY)).minusDays(7);
        }

        System.out.println(thirdTuesday);
        return thirdTuesday.toEpochDay();
    }

    public static long getNextThirdTuesdayWithTime(int hour, int min){
        LocalDate thirdTuesday = LocalDate
                .now()
                .with(lastDayOfMonth())
                .with(previous(DayOfWeek.TUESDAY)).minusDays(7);

        if(LocalDate.now().isAfter(thirdTuesday)){
            thirdTuesday = LocalDate.now()
                    .plusMonths(1)
                    .with(lastDayOfMonth())
                    .with(previous(DayOfWeek.TUESDAY)).minusDays(7);
        }

        return thirdTuesday.atTime(hour, min).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static long getNextDayWithTime(int hour, int min){
        LocalDateTime time = LocalDate.now().atTime(hour, min);

        if(LocalDateTime.now().isAfter(time)){
            time.plusDays(1);
        }

        return time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
