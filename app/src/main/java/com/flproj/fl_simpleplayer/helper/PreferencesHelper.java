package com.flproj.fl_simpleplayer.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hieunt on 3/15/18.
 */

public class PreferencesHelper {

    private static final String PROPERTY_ASKED_FOR_RATING       = "AskedForRating";
    private static final String PROPERTY_PLAYLIST                = "Playlist2";
    private static final String PROPERTY_EPISODE                = "Episode2";
    private static final String PROPERTY_EPISODE_SPEED               = "epSpeed";
    private static final String PREFERENCES = "PremoPreferences";

    private SharedPreferences mPreferences;

    private static PreferencesHelper sInstance;

    public static PreferencesHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new PreferencesHelper(context);
        }
        return sInstance;
    }

    /**
     * Creates a new PreferenceManager
     * @param context context
     */
    private PreferencesHelper(Context context) {
        mPreferences = context.getSharedPreferences(PREFERENCES, 0);

    }

    /**
     * Removes the property with the specified key
     * @param key
     */
    public void remove(String key) {
        mPreferences.edit().remove(key).apply();
    }

    /**
     * Deletes all preference data
     */
    public void clear() {
        mPreferences.edit().clear().apply();
    }

    public boolean hasAskedForRating() {
        return mPreferences.getBoolean(PROPERTY_ASKED_FOR_RATING, false);
    }

    public void setAskedForRating() {
        mPreferences.edit().putBoolean(PROPERTY_ASKED_FOR_RATING, true).apply();
    }

    public int getLastPlaylistId() {
        return mPreferences.getInt(PROPERTY_PLAYLIST, -1);
    }

    public void setLastPlaylistId(int playlist) {
        mPreferences.edit().putInt(PROPERTY_PLAYLIST, playlist).apply();
    }

    public int getLastEpisodeId() {
        return mPreferences.getInt(PROPERTY_EPISODE, -1);
    }

    public void setLastEpisodeId(int id) {
        mPreferences.edit().putInt(PROPERTY_EPISODE, id).apply();
    }

    public float getLastEpisodeSpeed() {
        return mPreferences.getFloat(PROPERTY_EPISODE_SPEED, 1.0f);
    }

    public void setLastEpisodeSpeed(float speed) {
        mPreferences.edit().putFloat(PROPERTY_EPISODE_SPEED, speed).apply();
    }
}
