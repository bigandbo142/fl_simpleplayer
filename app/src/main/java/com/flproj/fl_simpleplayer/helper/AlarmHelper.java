package com.flproj.fl_simpleplayer.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;

import com.flproj.fl_simpleplayer.pojos.AlarmSetting;
import com.flproj.fl_simpleplayer.services.AlarmReceiver;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by hieunt on 3/18/18.
 */

public class AlarmHelper {

    public static AlarmSetting addAlarm(AlarmSetting setting, Context context){
        AlarmManager mgrAlarm = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("alarm_id", setting.getUid());
        PendingIntent operation = PendingIntent.getBroadcast(context, setting.getUid(), intent,  PendingIntent.FLAG_UPDATE_CURRENT );

        mgrAlarm.cancel(operation);

        if(setting.isRepeat()) {
            switch (setting.getRepeatType()) {
                case 0: // Daily
                    mgrAlarm.setExact(AlarmManager.RTC_WAKEUP, Long.valueOf(setting.getTriggerTime()), operation);
                    mgrAlarm.setRepeating(AlarmManager.RTC_WAKEUP, Long.valueOf(setting.getTriggerTime()), AlarmManager.INTERVAL_DAY, operation);
                    break;
                case 1: // first tuesday of month
                    setting.setTriggerTime(String.valueOf(DatetimeHelper.getNextFirstTuesdayWithTime(setting.getHour(), setting.getMinute())));
                    mgrAlarm.setExact(AlarmManager.RTC_WAKEUP, Long.valueOf(setting.getTriggerTime()), operation);
                    break;
                case 2: // third tuesday of month
                    setting.setTriggerTime(String.valueOf(DatetimeHelper.getNextThirdTuesdayWithTime(setting.getHour(), setting.getMinute())));
                    mgrAlarm.setExact(AlarmManager.RTC_WAKEUP, Long.valueOf(setting.getTriggerTime()), operation);
                    break;
            }
        }else{
            mgrAlarm.setExact(AlarmManager.RTC_WAKEUP, Long.valueOf(setting.getTriggerTime()), operation);
        }

        return setting;
    }

    public static void cancelAlarm(Context context, AlarmSetting setting){
        AlarmManager mgrAlarm = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("alarm_id", setting.getUid());
        PendingIntent operation = PendingIntent.getBroadcast(context, setting.getUid(), intent,  PendingIntent.FLAG_CANCEL_CURRENT );

        mgrAlarm.cancel(operation);
    }

}
