package com.flproj.fl_simpleplayer.helper;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.session.PlaybackState;
import android.support.v4.app.TaskStackBuilder;

import com.flproj.fl_simpleplayer.NowPlayingActivity;
import com.flproj.fl_simpleplayer.pojos.Episode;
import com.flproj.fl_simpleplayer.services.SimplePlayerService;

/**
 * Created by hieunt on 3/14/18.
 */

public class PendingIntentHelper {

    public static final int REQ_CODE_NEW_EPISODE_DELETE = 1;
    public static final int REQ_CODE_PLAY_EPISODE = 2;
    public static final int REQ_CODE_OPEN_PREMO_ACTIVITY = 3;
    public static final int REQ_CODE_STOP_SERVICE = 4;
    public static final int REQ_CODE_SEEK_FORWARD = 5;
    public static final int REQ_CODE_SEEK_BACKWARD = 6;
    public static final int REQ_CODE_PLAY_OR_PAUSE = 7;
    public static final int REQ_CODE_OPEN_NOW_PLAYING_ACTIVITY = 8;
    public static final int REQ_CODE_NEW_DOWNLOAD_DELETE = 9;
    public static final int REQ_CODE_MEDIA_BUTTON_RECEIVER = 10;
    public static final int REQ_CODE_SHOW_EPISODE_INFO = 11;


    public static PendingIntent getPlayEpisodeIntent(Context context, int episodeId) {
        // this is so when the user presses back, we go back to the last activity (or app)
        Intent playIntent = new Intent(context, NowPlayingActivity.class);
//        playIntent.putExtra(NowPlayingActivity.PARAM_EPISODE_ID, episodeId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(playIntent);
        return stackBuilder.getPendingIntent(REQ_CODE_PLAY_EPISODE,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getStopServiceIntent(Context context) {
        return PendingIntent.getService(context, REQ_CODE_STOP_SERVICE,
                new Intent(context, SimplePlayerService.class)
                        .setAction(SimplePlayerService.ACTION_STOP_SERVICE),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }


    public static PendingIntent getSeekForwardIntent(Context context) {
        return PendingIntent.getService(context, REQ_CODE_SEEK_FORWARD,
                new Intent(context, SimplePlayerService.class)
                        .setAction(SimplePlayerService.ACTION_SEEK_FORWARD),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getSeekBackwardIntent(Context context) {
        return PendingIntent.getService(context, REQ_CODE_SEEK_BACKWARD,
                new Intent(context, SimplePlayerService.class)
                        .setAction(SimplePlayerService.ACTION_SEEK_BACKWARD),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getPlayOrPauseIntent(Context context, int state) {
        String playOrPauseAction;

        if (state == PlaybackState.STATE_PLAYING) {
            playOrPauseAction = SimplePlayerService.ACTION_PAUSE;
        } else {
            playOrPauseAction = SimplePlayerService.ACTION_RESUME_PLAYBACK;
        }

        return PendingIntent.getService(context, REQ_CODE_PLAY_OR_PAUSE,
                new Intent(context, SimplePlayerService.class).setAction(playOrPauseAction),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getOpenNowPlayingIntent(Context context, Episode episode) {
        // this is so when the user presses back, we go back to the last activity (or app)
        Intent playIntent = new Intent(context, NowPlayingActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(playIntent);
        return stackBuilder.getPendingIntent(REQ_CODE_OPEN_NOW_PLAYING_ACTIVITY,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

//    public static PendingIntent getMediaButtonReceiverIntent(Context context) {
//        ComponentName mediaButtonReceiver = new ComponentName(context, RemoteControlReceiver.class);
//        return PendingIntent.getBroadcast(context, REQ_CODE_MEDIA_BUTTON_RECEIVER,
//                new Intent().setComponent(mediaButtonReceiver), PendingIntent.FLAG_UPDATE_CURRENT);
//    }

}
