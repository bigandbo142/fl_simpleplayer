package com.flproj.fl_simpleplayer.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by hieunt on 3/14/18.
 */

public class BroadcastHelper {

    public static final String INTENT_PROGRESS_UPDATE = "progressUpdate";
    public static final String INTENT_UI_UPDATE = "uiUpdate";
    public static final String INTENT_STATE_UPDATE = "stateUpdate";
    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_PATH = "path";
    public static final String EXTRA_NEXT_PATH = "nextPath";
    public static final String EXTRA_PREVIOUS_PATH = "previousPath";
    public static final String EXTRA_SPEED = "speed";
    public static final String EXTRA_PROGRESS = "progress";
    public static final String EXTRA_BUFFERED_PROGRESS = "bufferedProgress";
    public static final String EXTRA_DURATION = "duration";
    public static final String EXTRA_STATE = "state";

    private static void sendBroadcast(Context context, Intent intent) {

        if (!intent.getAction().contentEquals(INTENT_PROGRESS_UPDATE)) {
            Log.d("TEST888", "Broadcasting intent: " + intent.getAction());
        }
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void registerReceiver(Context context, BroadcastReceiver receiver, String... actions) {

        for (int i = 0; i < actions.length; i++) {
            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(actions[i]));
        }
    }

    public static void unregisterReceiver(Context context, BroadcastReceiver... receivers) {

        for (int i = 0; i < receivers.length; i++) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receivers[i]);
        }
    }

    public static void broadcastProgressUpdate(Context context, long progress, long bufferedProgress,
                                               long duration) {
        Intent intent = new Intent(INTENT_PROGRESS_UPDATE);
        intent.putExtra(EXTRA_PROGRESS, progress);
        intent.putExtra(EXTRA_BUFFERED_PROGRESS, bufferedProgress);
        intent.putExtra(EXTRA_DURATION, duration);
        sendBroadcast(context, intent);
    }

    public static void broadcastStateUpdate(Context context, int state) {
        Intent intent = new Intent(INTENT_STATE_UPDATE);
        intent.putExtra(EXTRA_STATE, state);
        sendBroadcast(context, intent);
    }

    public static void broadcastUIUpdate(Context context, String name, float playbackSpeed, String path, String nextPath, String previousPath) {
        Intent intent = new Intent(INTENT_UI_UPDATE);
        intent.putExtra(EXTRA_NAME, name);
        intent.putExtra(EXTRA_SPEED, playbackSpeed);
        intent.putExtra(EXTRA_PATH, path);
        intent.putExtra(EXTRA_NEXT_PATH, nextPath);
        intent.putExtra(EXTRA_PREVIOUS_PATH, previousPath);
        sendBroadcast(context, intent);
    }
}
