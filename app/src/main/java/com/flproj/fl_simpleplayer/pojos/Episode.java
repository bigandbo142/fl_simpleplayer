package com.flproj.fl_simpleplayer.pojos;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcelable;

import org.parceler.Parcel;

/**
 * Created by hieunt on 3/14/18.
 */
@Entity(tableName = "episode",foreignKeys =
@ForeignKey(entity=Playlist.class,parentColumns = "uid",childColumns = "playlist_id"))
public class Episode implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid")
    private int uid;
    @ColumnInfo(name = "local_path")
    private String path = "";
    @ColumnInfo(name = "progress")
    private int progress = 0;
    @ColumnInfo(name = "file_name")
    private String name = "";
    @ColumnInfo(name = "author")
    private String author = "";
    @ColumnInfo(name = "playlist_id")
    private int playlistId = 0;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Episode() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.uid);
        dest.writeString(this.path);
        dest.writeInt(this.progress);
        dest.writeString(this.name);
        dest.writeString(this.author);
        dest.writeInt(this.playlistId);
    }

    protected Episode(android.os.Parcel in) {
        this.uid = in.readInt();
        this.path = in.readString();
        this.progress = in.readInt();
        this.name = in.readString();
        this.author = in.readString();
        this.playlistId = in.readInt();
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        @Override
        public Episode createFromParcel(android.os.Parcel source) {
            return new Episode(source);
        }

        @Override
        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };
}
