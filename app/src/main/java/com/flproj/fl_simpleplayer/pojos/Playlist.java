package com.flproj.fl_simpleplayer.pojos;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by hieunt on 3/14/18.
 */
@Entity(tableName = "playlist")
public class Playlist implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid")
    private int uid;
    @ColumnInfo(name = "current_episode_id")
    private int currentEpisodeId;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCurrentEpisodeId() {
        return currentEpisodeId;
    }

    public void setCurrentEpisodeId(int currentEpisodeId) {
        this.currentEpisodeId = currentEpisodeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.uid);
        dest.writeInt(this.currentEpisodeId);
    }

    public Playlist() {
    }

    protected Playlist(Parcel in) {
        this.uid = in.readInt();
        this.currentEpisodeId = in.readInt();
    }

    public static final Parcelable.Creator<Playlist> CREATOR = new Parcelable.Creator<Playlist>() {
        @Override
        public Playlist createFromParcel(Parcel source) {
            return new Playlist(source);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };
}
