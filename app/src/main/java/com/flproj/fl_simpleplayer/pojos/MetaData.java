package com.flproj.fl_simpleplayer.pojos;

import android.database.Cursor;

/**
 * Created by hieunt on 3/17/18.
 */

public class MetaData {

    private String queryTtile;
    private String queryAlbum;
    private String queryArtist;
    private String queryDuration;
    private String filepath;
    private String _id;

    public MetaData(Cursor cursor) {
        queryTtile = cursor.getString(0);
        queryArtist = cursor.getString(1);
        queryAlbum = cursor.getString(2);
        queryDuration = cursor.getString(3);
        filepath = cursor.getString(4);
        _id = cursor.getString(5);
    }
}
