package com.flproj.fl_simpleplayer.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.flproj.fl_simpleplayer.data.AppDatabase;
import com.flproj.fl_simpleplayer.helper.BroadcastHelper;
import com.flproj.fl_simpleplayer.helper.PreferencesHelper;
import com.flproj.fl_simpleplayer.media.LocalMediaPlayer;
import com.flproj.fl_simpleplayer.media.MediaPlayer;
import com.flproj.fl_simpleplayer.media.MediaPlayerState;
import com.flproj.fl_simpleplayer.media.ProgressUpdateListener;
import com.flproj.fl_simpleplayer.pojos.Episode;
import com.flproj.fl_simpleplayer.pojos.Playlist;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by hieunt on 3/14/18.
 */

public class SimplePlayerService extends Service implements AudioManager.OnAudioFocusChangeListener, MediaPlayer.SimpleMediaPlayerListener, ProgressUpdateListener {

    private static final int MS_TO_REVERSE_ON_PAUSE         = 0;
    private static final float AUDIO_DUCK                   = 0.8f;
    private static final float AUDIO_INTERVAL                   = 0.1f;
    private static final long MEDIA_SESSION_ACTIONS =
            PlaybackState.ACTION_FAST_FORWARD |
                    PlaybackState.ACTION_REWIND |
                    PlaybackState.ACTION_SEEK_TO |
                    PlaybackState.ACTION_SKIP_TO_NEXT |
                    PlaybackState.ACTION_SKIP_TO_PREVIOUS;

    public static final String ACTION_PLAY_QUEUE        = "playQueue";
    public static final String ACTION_PLAY_EPISODE      = "playNew";
    public static final String ACTION_PLAY_PLAYLIST     = "playPlaylist";
    public static final String ACTION_RESUME_PLAYLIST     = "resumePlaylist";
    public static final String ACTION_RESUME_PLAYBACK   = "play";
    public static final String ACTION_PAUSE             = "pause";
    public static final String ACTION_STOP             = "stop";
    public static final String ACTION_RESET_EPISODE             = "reset";
    public static final String ACTION_REPEAT_EPISODE             = "repeat";
    public static final String ACTION_SEEK_FORWARD      = "seekForward";
    public static final String ACTION_SEEK_BACKWARD     = "seekBackward";
    public static final String ACTION_SEEK_TO           = "seekTo";
    public static final String ACTION_STOP_SERVICE      = "stopService";
    public static final String ACTION_VOLUME_UP      = "volumeUp";
    public static final String ACTION_VOLUME_DOWN      = "volumeDown";
    public static final String ACTION_UPDATE_WIDGET     = "updateWidget";
    public static final String ACTION_SLEEP_TIMER       = "sleepTimer";
    public static final String ACTION_SET_SPEED         = "com.mainmethod.premofm.setPlaybackSpeed";

    public static final String PARAM_PLAYLIST           = "playlist";
    public static final String PARAM_QUEUE_DIRECTION           = "queueDirection";
    public static final String PARAM_EPISODE_ID         = "episodeId";
    public static final String PARAM_EPISODE_OBJ         = "episodeObj";
    public static final String PARAM_SEEK_MS            = "seekMs";
    public static final String PARAM_PLAYBACK_SPEED     = "playbackSpeed";

    private AudioManager mAudioManager;
    private int mMediaPlayerState;
    private PlaybackState mPlaybackState;
    private MediaSession mMediaSession;
    private LocalMediaPlayer mMediaPlayer;

    private Episode mCurrentEpisode;
    private Playlist mCurrentPlaylist;
    private List<Episode> episodesInList = new ArrayList<>();
    private float mCurrentSpeed = 1.0f;
    private boolean isRepeatCurrentEpisode = false;

    private boolean mServiceBound = false;

    private final IBinder mBinder = new ServiceBinder();

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */
    public class ServiceBinder extends Binder {
        public SimplePlayerService getService() {
            return SimplePlayerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mMediaPlayerState = MediaPlayerState.STATE_IDLE;

        // set default playback state
        mPlaybackState = new PlaybackState.Builder()
                .setState(PlaybackState.STATE_NONE, 0, 1.0f)
                .build();

        // setup our media session
        mMediaSession = new MediaSession(this, SimplePlayerService.class.getSimpleName());
//        mMediaSession.setMediaButtonReceiver(PendingIntentHelper.getMediaButtonReceiverIntent(this));
        mMediaSession.setCallback(new MediaSessionCallback());
        mMediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setActive(true);
        mMediaSession.setPlaybackState(mPlaybackState);

        // set up our broadcast receiver for receiving headset and button events
//        mHeadsetReceiver = new HeadsetReceiver();
    }

    @Override
    public void onDestroy() {
        destroyMediaPlayer();
//        mMediaNotificationManager.stopNotification();
        mMediaSession.release();
        super.onDestroy();


    }

    /******************************
     * Service bind functions
     ******************************/

    @Override
    public boolean onUnbind(Intent intent) {

        if (mMediaPlayerState == MediaPlayerState.STATE_IDLE) {
            stopSelf();
        }
        mServiceBound = false;
        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        mServiceBound = true;
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();

            Log.d("TEST888", "OnStartCommand intent action: " + action);

            switch (action) {
                case ACTION_PLAY_QUEUE:
                    int queueDirection = intent.getIntExtra(PARAM_QUEUE_DIRECTION, 0);
                    if(queueDirection == 0) // play next
                        playNextEpisode();
                    else // play previous
                        playPreviousEpisode();
                    break;
                case ACTION_PLAY_EPISODE:
                    Episode episode = intent.getParcelableExtra(PARAM_EPISODE_OBJ);
                    if (episode != null) {
                        play(episode, true);
                    }else{
                        play(mCurrentEpisode, true);
                    }
                    break;
                case ACTION_RESET_EPISODE:
                    if(mCurrentEpisode !=  null){
                        seekPlayerTo(0);
                    }
                    break;

                case ACTION_VOLUME_UP:
                    mMediaPlayer.changeVolume(AUDIO_INTERVAL);
                    break;
                case ACTION_VOLUME_DOWN:
                    mMediaPlayer.changeVolume((-1) * AUDIO_INTERVAL);
                    break;
                case ACTION_REPEAT_EPISODE:
                    if(mCurrentEpisode !=  null){
                        isRepeatCurrentEpisode = !isRepeatCurrentEpisode;
                    }
                    break;
                case ACTION_PLAY_PLAYLIST:
                    mMediaPlayerState = MediaPlayerState.STATE_IDLE;
                    int playlistId = intent.getIntExtra(PARAM_PLAYLIST, -1);
                    mCurrentPlaylist = new Playlist();
                    mCurrentPlaylist.setUid(playlistId);
                    Log.d("TEST888", "OnStartCommand intent playlistId: " + playlistId);
                    if(playlistId >= 0){
                        Single.fromCallable(() -> AppDatabase.getAppDatabase(getApplicationContext())
                                    .episodeDao().getAllByPlayListId(playlistId))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(episodes -> {
                                    if(episodes != null && episodes.size() > 0){
                                        episodesInList.clear();
                                        episodesInList.addAll(episodes);
                                        mCurrentEpisode = episodesInList.get(0);
                                        Log.d("TEST888", "OnStartCommand intent mCurrentEpisode: " + mCurrentEpisode.getName());
                                        play(mCurrentEpisode, true);
                                        PreferencesHelper.getInstance(getApplicationContext())
                                                .setLastPlaylistId(playlistId);


                                    }
                                });
                    }

                    break;
                case ACTION_RESUME_PLAYLIST:

                    int listId = PreferencesHelper.getInstance(getApplicationContext())
                            .getLastPlaylistId();
                    int epId = PreferencesHelper.getInstance(getApplicationContext())
                            .getLastEpisodeId();

                    if(listId != -1 && epId != -1){
                        mCurrentPlaylist = new Playlist();
                        mCurrentPlaylist.setUid(listId);
                        if(listId >= 0){
                            Single.fromCallable(() -> AppDatabase.getAppDatabase(getApplicationContext())
                                    .episodeDao().getAllByPlayListId(listId))
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(episodes -> {
                                        if(episodes != null && episodes.size() > 0){
                                            episodesInList.clear();
                                            episodesInList.addAll(episodes);
                                            for (Episode epObj : episodesInList){
                                                if(epObj.getUid() == epId){
                                                    mCurrentEpisode = epObj;
                                                    break;
                                                }
                                            }

                                            play(mCurrentEpisode, true);
                                            PreferencesHelper.getInstance(getApplicationContext())
                                                    .setLastPlaylistId(listId);

                                            try {
                                                boolean foundNextEpisode = false;
                                                String nextPath = "";
                                                String previousPath = "";
                                                for (Episode ep : episodesInList) {
                                                    if (foundNextEpisode) {
                                                        nextPath = ep.getPath();
                                                        break;
                                                    }
                                                    if (ep.getUid() == mCurrentEpisode.getUid()) {
                                                        foundNextEpisode = true;
                                                    }else{
                                                        previousPath = ep.getPath();
                                                    }
                                                }

                                                BroadcastHelper.broadcastUIUpdate(this, mCurrentEpisode.getName(), mCurrentSpeed, mCurrentEpisode.getPath(), nextPath, previousPath);
                                            }catch (Exception ex){
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                        }
                    }
                    break;
                case ACTION_RESUME_PLAYBACK:

                    if (mMediaPlayerState != MediaPlayerState.STATE_PLAYING) {
                        play(null, true);
                    }
                    break;
                case ACTION_PAUSE:
                    pause();
                    break;
                case ACTION_STOP:
                    stopPlayback();
                    break;
                case ACTION_SEEK_FORWARD:
                    seekForward();
                    break;
                case ACTION_SEEK_BACKWARD:
                    seekBackward();
                    break;
                case ACTION_SEEK_TO:
                    int seekMs = intent.getIntExtra(PARAM_SEEK_MS, 30);
                    seekTo(seekMs);
                    break;
                case ACTION_STOP_SERVICE:
                    endPlayback(true);

                    if (!mServiceBound) {
                        stopSelf();
                    }
                    break;
                case ACTION_SET_SPEED:
                    mCurrentSpeed = intent.getFloatExtra(PARAM_PLAYBACK_SPEED, 1.0f);
                    changePlaybackSpeed(mCurrentSpeed);
                    break;
            }

        }
        return START_NOT_STICKY;
    }

    private void destroyMediaPlayer() {
        if (mMediaPlayer == null) {
            return;
        }
//        endUpdateTask();
        endPlayback(true);
        mMediaPlayerState = MediaPlayerState.STATE_IDLE;
        updatePlaybackState(mMediaPlayerState);
        mMediaPlayer.tearDown();
        mMediaPlayer = null;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }

    @Override
    public void onProgressUpdate(long progress, long bufferedProgress, long duration) {
        updateProgressOfEpisode(mCurrentEpisode, (int) progress);
        BroadcastHelper.broadcastProgressUpdate(this, progress, bufferedProgress, duration);
    }

    @Override
    public void onStateChanged(int state) {
        updatePlaybackState(state);
        mMediaPlayerState = state;

        switch (state) {
            case MediaPlayerState.STATE_CONNECTING:
                break;
            case MediaPlayerState.STATE_ENDED:
//                mMediaPlayer = new LocalMediaPlayer(this, this, this);
                BroadcastHelper.broadcastStateUpdate(this, MediaPlayerState.STATE_ENDED);
                updateProgressOfEpisode(mCurrentEpisode, 0);
                if(isRepeatCurrentEpisode){
                    playRepeatEpisode();
                }else {
                    if (!isEndList())
                        playNextEpisode();
                    else{
                        endPlayback(true);
                    }
                }
                break;
            case MediaPlayerState.STATE_IDLE:
//                updateProgressOfEpisode(mCurrentEpisode, 0);
//                mMediaPlayer = new LocalMediaPlayer(this, this, this);
//                updateProgressOfEpisode(mCurrentEpisode, 0);
                BroadcastHelper.broadcastStateUpdate(this, MediaPlayerState.STATE_IDLE);
                break;
            case MediaPlayerState.STATE_PLAYING:
                BroadcastHelper.broadcastStateUpdate(this, MediaPlayerState.STATE_PLAYING);
                break;
            case MediaPlayerState.STATE_PAUSED:
                BroadcastHelper.broadcastStateUpdate(this, MediaPlayerState.STATE_PAUSED);
                break;
        }
//        updateWidget();
//        String serverId = mCurrentEpisode != null ? mCurrentEpisode.getGeneratedId() : "";
//        BroadcastHelper.broadcastPlayerStateChange(this, mMediaPlayerState, serverId);
    }

    /******************************
     * Media Player controls
     ******************************/

    private void startPlayback(Episode episode, boolean playImmediately) {
        // request audio focus
        int result = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.d("TEST888", "Gsf111111sefesf");
            mCurrentEpisode = episode;
//            registerReceivers();
            mMediaPlayer.loadEpisode(episode);
            mMediaPlayer.startPlayback(playImmediately);

            mCurrentPlaylist.setCurrentEpisodeId(episode.getUid());

            PreferencesHelper.getInstance(getApplicationContext())
                    .setLastEpisodeId(episode.getUid());
            Single.fromCallable(() -> {
                try{
                    AppDatabase.getAppDatabase(getApplicationContext())
                            .playlistDao().update(mCurrentPlaylist);

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                return true;
            }).subscribeOn(Schedulers.io())
                    .subscribe(aBoolean -> {

                    }, throwable -> {
                        throwable.printStackTrace();
                    });

//            mMediaPlayer.setPlaybackSpeed(
//                    AppPrefHelper.getInstance(this).getPlaybackSpeed(
//                            mCurrentEpisode.getChannelGeneratedId()));


            boolean foundNextEpisode = false;
            String nextPath = "";
            String prevPath = "";
            for (Episode ep : episodesInList) {
                if(foundNextEpisode){
                    nextPath = ep.getPath();
                    break;
                }
                if (ep.getUid() == mCurrentEpisode.getUid()){
                    foundNextEpisode = true;
                }else{
                    prevPath = ep.getPath();
                }
            }

            BroadcastHelper.broadcastUIUpdate(this, mCurrentEpisode.getName(), mCurrentSpeed, mCurrentEpisode.getPath(), nextPath, prevPath);
            onStateChanged(MediaPlayerState.STATE_PLAYING);
        } else {
            Log.d("TEST888", "Gsfsefesf");
        }
    }

    private void stopPlayback() {
        mMediaPlayer.pausePlayback();

        Episode episode = mCurrentEpisode;
        updateProgressOfEpisode(episode, 0);

        mCurrentEpisode = episodesInList.get(0);
        mCurrentEpisode.setProgress(0);
//
        BroadcastHelper.broadcastProgressUpdate(this, 0, 0, 0);

        boolean foundNextEpisode = false;
        String nextPath = "";
        String prevPath = "";
        for (Episode ep : episodesInList) {
            if(foundNextEpisode){
                nextPath = ep.getPath();
                break;
            }
            if (ep.getUid() == mCurrentEpisode.getUid()){
                foundNextEpisode = true;
            }else{
                prevPath = ep.getPath();
            }
        }
        BroadcastHelper.broadcastUIUpdate(this, mCurrentEpisode.getName(), mCurrentSpeed, mCurrentEpisode.getPath(), nextPath, prevPath);

        onStateChanged(MediaPlayerState.STATE_IDLE);
    }

    private void pausePlayback() {
        mMediaPlayer.pausePlayback();
        onStateChanged(MediaPlayerState.STATE_PAUSED);
    }

    private void endPlayback(boolean cancelNotification) {
//        updateEpisode(MediaPlayerState.STATE_IDLE);
//        unregisterReceivers();

        if (cancelNotification) {
//            mMediaNotificationManager.stopNotification();
        }
//        mAudioManager.abandonAudioFocus(this);
        mMediaPlayer.pausePlayback();
        mCurrentEpisode = null;
        onStateChanged(MediaPlayerState.STATE_IDLE);
    }

    private void seekPlayerTo(long seekTo) {
        long currentPosition = mMediaPlayer.getCurrentPosition();

        if (seekTo < 0) {
            seekTo = 0;
        } else if (seekTo > mMediaPlayer.getDuration()) {
            seekTo = mMediaPlayer.getDuration();
        }
        mMediaPlayer.seekTo(seekTo);
    }

    private void seekPlayerBy(long msec) {
        long currentPosition = mMediaPlayer.getCurrentPosition();
        long duration = mMediaPlayer.getDuration();
        long seekTo = currentPosition + msec;

        if (seekTo < 0) {
            seekTo = 0;
        } else if (seekTo > duration) {
            seekTo = duration;
        }
        mMediaPlayer.seekTo(seekTo);
    }

    /**
     * Returns the progress of the player
     * @return progress
     */
    private long getProgress() {
        long progress = -1;

        if (mMediaPlayer == null) {
            return progress;
        }

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PLAYING:
            case MediaPlayerState.STATE_PAUSED:
                progress = mMediaPlayer.getCurrentPosition();
                break;
        }
        return progress;
    }

    /**
     * Returns the length of the track
     * @return length of current track
     */
    private long getDuration() {
        long duration = -1;

        if (mMediaPlayer == null) {
            return duration;
        }

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PLAYING:
            case MediaPlayerState.STATE_PAUSED:
                duration = mMediaPlayer.getDuration();
                break;
        }
        return duration;
    }

    /**
     * Returns the time remaining
     * @return duration left
     */
    private long getDurationLeft() {
        long timeLeft = -1;

        if (mMediaPlayer == null) {
            return timeLeft;
        }

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PLAYING:
            case MediaPlayerState.STATE_PAUSED:
                long duration = getDuration();
                long progress = getProgress();
                timeLeft = duration - progress;
                break;
        }
        return timeLeft;
    }

    public int getPlaybackState() {
        return mPlaybackState.getState();
    }

    private final class MediaSessionCallback extends MediaSession.Callback {

        @Override
        public void onPlayFromSearch(String query, Bundle extras) {
            int episodeId = extras.getInt(PARAM_EPISODE_ID, -1);

            if (episodeId != -1) {
//                play(EpisodeModel.getEpisodeById(PodcastPlayerService.this, episodeId), true);
            }
        }

        @Override
        public void onSeekTo(long pos) {
//            seekTo(pos);
        }

        @Override
        public void onPlay() {
//            play(-1);
        }

        @Override
        public void onPause() {
//            pause();
        }

        @Override
        public void onFastForward() {
//            seekForward();
        }

        @Override
        public void onRewind() {
//            seekBackward();
        }

        @Override
        public void onSkipToNext() {
//            onFastForward();
        }

        @Override
        public void onSkipToPrevious() {
//            onRewind();
        }
    }

    private void updatePlaybackState(int stateVal) {
        // get current position
        long currentPosition = mMediaPlayer.getCurrentPosition();
        int playbackState;
        long actions = MEDIA_SESSION_ACTIONS;

        switch (stateVal) {
            case MediaPlayerState.STATE_CONNECTING:
                playbackState = PlaybackState.STATE_CONNECTING;
                actions |= PlaybackState.ACTION_PAUSE;
                break;
            case MediaPlayerState.STATE_IDLE:
                playbackState = PlaybackState.STATE_NONE;
                actions |= PlaybackState.ACTION_PLAY;
                break;
            case MediaPlayerState.STATE_ENDED:
                playbackState = PlaybackState.STATE_STOPPED;
                actions |= PlaybackState.ACTION_PLAY;
                break;
            case MediaPlayerState.STATE_PAUSED:
                playbackState = PlaybackState.STATE_PAUSED;
                actions |= PlaybackState.ACTION_PLAY;
                break;
            case MediaPlayerState.STATE_PLAYING:
                playbackState = PlaybackState.STATE_PLAYING;
                actions |= PlaybackState.ACTION_PAUSE;
                break;
            default:
                playbackState = PlaybackState.STATE_NONE;
                actions |= PlaybackState.ACTION_PLAY;
                break;
        }

        // create new playback state and add it to the media session
        mPlaybackState = new PlaybackState.Builder()
                .setState(playbackState, currentPosition, 1.0f)
                .setActions(actions)
                .build();

        // update current episode
        Bundle extras = new Bundle();

        if (mCurrentEpisode != null) {
//            extras.putInt(PARAM_EPISODE_ID, mCurrentEpisode.getId());
        }

        // add new properties to the media session
        mMediaSession.setPlaybackState(mPlaybackState);
        mMediaSession.setExtras(extras);
    }

    /**
     * Plays an episode
     * Will resume from pause if episode is null and we are paused
     * @param episode to play
     */
    private void play(Episode episode, boolean playImmediately) {
        Log.d("TEST888", "Play called " + mMediaPlayerState);

        if (mMediaPlayer == null) {
            mMediaPlayer = new LocalMediaPlayer(this, this, this);
        }

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_CONNECTING:
            case MediaPlayerState.STATE_PLAYING:
                // playing an episode, true if the episode to play is a different one
                if (episode != null) {
//                    endPlayback(false);
                    startPlayback(episode, playImmediately);
                }
                break;
            case MediaPlayerState.STATE_PAUSED:
                mMediaPlayer.resumePlayback();
                break;
            case MediaPlayerState.STATE_ENDED:
            case MediaPlayerState.STATE_IDLE:
                // stopped or uninitialized, so we need to start from scratch

                if (episode != null) {
                    Log.d("TEST888", "Player is stopped/uninitialized, episode cannot be null111111");
                    startPlayback(episode, playImmediately);
                } else {
                    Log.d("TEST888", "Player is stopped/uninitialized, episode cannot be null");
                }
                break;
            default:
                Log.d("TEST888", "Trying to pause an episode, but player is in state: " + mMediaPlayerState);
                break;
        }
    }

    /**
     * Will pause the player if it's playing
     */
    private void pause() {

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PLAYING:
                // we paused, resume playing state
                pausePlayback();
                break;
            default:
                Log.d("TEST888", "Trying to pause an episode, but player is in state: " + mMediaPlayerState);
                break;
        }
    }

    private void changePlaybackSpeed(float speed) {

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PAUSED:
            case MediaPlayerState.STATE_PLAYING:
                mMediaPlayer.setPlaybackSpeed(speed);
                onStateChanged(mMediaPlayerState);
                break;
        }
    }

    /**
     * Seek forward by the user's preferred amount
     */
    private void seekForward() {

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PAUSED:
            case MediaPlayerState.STATE_PLAYING:
                long seekTo = 30 * 1000;
                seekPlayerBy(seekTo);
                break;
            default:
                Log.d("TEST888", "Trying to pause an episode, but player is in state: " + mMediaPlayerState);
                break;
        }
    }

    /**
     * Seek forward by the user's preferred amount
     */
    private void seekBackward() {

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PAUSED:
            case MediaPlayerState.STATE_PLAYING:
                long seekTo = 30 * 1000;
                seekPlayerBy(-seekTo);
                break;
            default:
                Log.d("TEST888", "Trying to pause an episode, but player is in state: " + mMediaPlayerState);
                break;
        }
    }

    private void seekTo(long seekTo) {

        switch (mMediaPlayerState) {
            case MediaPlayerState.STATE_PAUSED:
            case MediaPlayerState.STATE_PLAYING:
                seekPlayerTo(seekTo);
                break;
            default:
                Log.d("TEST888", "Trying to pause an episode, but player is in state: " + mMediaPlayerState);
                break;
        }
    }

    public static void sendIntent(Context context, String action, Episode episode) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(action);

        if(episode != null)
            intent.putExtra(PARAM_EPISODE_OBJ, episode);
        context.startService(intent);
    }

    public static void sendIntent(Context context, String action, int seekTo) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(action);

        intent.putExtra(PARAM_SEEK_MS, seekTo);
        context.startService(intent);
    }

    public static void changePlaybackSpeed(Context context, float speed) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_SET_SPEED);
        intent.putExtra(PARAM_PLAYBACK_SPEED, speed);
        context.startService(intent);
    }

    public static void resumeLastPlaylist(Context context) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_RESUME_PLAYLIST);
        context.startService(intent);
    }

    public static void sendEventRunPlaylist(Context context, int playlistId) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_PLAY_PLAYLIST);
        intent.putExtra(PARAM_PLAYLIST, playlistId);
        context.startService(intent);
    }

    public static void toggleRepeatCurrentEpisode(Context context) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_REPEAT_EPISODE);
        context.startService(intent);
    }

    public static void replayCurrentEpisode(Context context) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_RESET_EPISODE);
        context.startService(intent);
    }


    public static void changeEpisodeInPlaylist(Context context, boolean isNext) {
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(ACTION_PLAY_QUEUE);
        intent.putExtra(PARAM_QUEUE_DIRECTION, isNext ? 0 : 1);
        context.startService(intent);
    }

    public static void changeVolume(Context context, boolean up){
        Intent intent = new Intent(context, SimplePlayerService.class);
        intent.setAction(up ? ACTION_VOLUME_UP : ACTION_VOLUME_DOWN);
        context.startService(intent);
    }

    private void updateProgressOfEpisode(Episode episode, int progress){
        Single.fromCallable(() -> {
            try{
                episode.setProgress((int)progress);
                AppDatabase.getAppDatabase(getApplicationContext())
                        .episodeDao()
                        .update(episode);
            }catch (Exception ex){
                ex.printStackTrace();
            }

            return true;
        }).subscribeOn(Schedulers.io())
                .subscribe(aBoolean -> {

                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void playNextEpisode(){
        if(episodesInList != null){
            if(mCurrentEpisode == null){
                if(episodesInList.size() > 0){
                    mCurrentEpisode = episodesInList.get(0);
                }
            }else {
                Episode oldEpisode = mCurrentEpisode;
                boolean foundNextEpisode = false;
                for (Episode ep : episodesInList) {
                    if(foundNextEpisode){
                        mCurrentEpisode = ep;
                        break;
                    }
                    if (ep.getUid() == mCurrentEpisode.getUid()){
                        foundNextEpisode = true;
                    }
                }

                if(oldEpisode.getUid() != mCurrentEpisode.getUid()){
                    updateProgressOfEpisode(oldEpisode, 0);
                }
            }

            play(mCurrentEpisode, true);
        }
    }

    private void playRepeatEpisode(){
        if(mCurrentEpisode != null){
            play(mCurrentEpisode, true);
        }
    }

    private boolean isEndList(){
        if(episodesInList != null && episodesInList.size() > 0){
            if(mCurrentEpisode == null){
                return true;
            }else{
                if(mCurrentEpisode.getUid() == episodesInList.get(episodesInList.size() - 1).getUid()){
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            return true;
        }
    }

    private void playPreviousEpisode(){
        if(episodesInList != null){
            if(mCurrentEpisode == null){
                if(episodesInList.size() > 0){
                    mCurrentEpisode = episodesInList.get(0);
                }
            }else {
                Episode oldEpisode = mCurrentEpisode;
                Episode tmpEpisode = null;
                for (Episode ep : episodesInList) {

                    if (ep.getUid() == mCurrentEpisode.getUid()){
                        break;
                    }

                    tmpEpisode = ep;
                }

                if(tmpEpisode !=  null){
                    mCurrentEpisode = tmpEpisode;
                }

                if(oldEpisode.getUid() != mCurrentEpisode.getUid()){
                    updateProgressOfEpisode(oldEpisode, 0);
                }
            }

            play(mCurrentEpisode, true);
        }
    }
}
