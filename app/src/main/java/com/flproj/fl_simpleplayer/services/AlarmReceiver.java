package com.flproj.fl_simpleplayer.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.flproj.fl_simpleplayer.BuildConfig;
import com.flproj.fl_simpleplayer.R;
import com.flproj.fl_simpleplayer.data.AppDatabase;
import com.flproj.fl_simpleplayer.helper.AlarmHelper;
import com.flproj.fl_simpleplayer.pojos.AlarmSetting;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.flproj.fl_simpleplayer.helper.PendingIntentHelper.REQ_CODE_STOP_SERVICE;

/**
 * Created by hieunt on 3/18/18.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int alarmId = intent.getIntExtra("alarm_id", -1);
        if(alarmId > 0){
            Single.fromCallable(() -> AppDatabase.getAppDatabase(context).alarmSettingDao().findById(alarmId))
                    .map(setting -> {
                        if(setting != null && setting.isOn()) {
                            if (setting.isRepeat()) {
                                if (setting.getRepeatType() != 0) {
                                    AlarmSetting newSetting = AlarmHelper.addAlarm(setting, context);
                                    AppDatabase.getAppDatabase(context).alarmSettingDao().update(newSetting);
                                    return newSetting;
                                }
                            }
                        }
                        return setting;
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(setting -> {
                        AlarmPlayerService.triggerAlarm(context, setting.getPath());
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        }
    }
}
