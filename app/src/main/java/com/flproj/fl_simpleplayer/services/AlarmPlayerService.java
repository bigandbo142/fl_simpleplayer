package com.flproj.fl_simpleplayer.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.flproj.fl_simpleplayer.R;
import com.flproj.fl_simpleplayer.media.LocalMediaPlayer;
import com.flproj.fl_simpleplayer.media.MediaPlayer;
import com.flproj.fl_simpleplayer.media.ProgressUpdateListener;
import com.flproj.fl_simpleplayer.pojos.Episode;

import static com.flproj.fl_simpleplayer.helper.PendingIntentHelper.REQ_CODE_STOP_SERVICE;

/**
 * Created by hieunt on 3/18/18.
 */

public class AlarmPlayerService extends Service {

    private LocalMediaPlayer mMediaPlayer;
    private final IBinder mBinder = new AlarmServiceBinder();

    public static final String ACTION_PLAY_ALARM        = "playAlarm";
    public static final String ACTION_STOP_ALARM        = "stopAlarm";
    public static final String PARAM_EPISODE_PATH         = "episodePath";

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaPlayer = new LocalMediaPlayer(new MediaPlayer.SimpleMediaPlayerListener() {
            @Override
            public void onStateChanged(int state) {

            }
        }, new ProgressUpdateListener() {
            @Override
            public void onProgressUpdate(long progress, long bufferedProgress, long duration) {

            }
        }, this);
    }

    @Override
    public void onDestroy() {
        destroyMediaPlayer();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_PLAY_ALARM:
                    Episode episode = new Episode();
                    episode.setPath(intent.getStringExtra(PARAM_EPISODE_PATH));
                    mMediaPlayer.loadEpisode(episode);
                    mMediaPlayer.startPlayback(true);
                    break;
                case ACTION_STOP_ALARM:
                    mMediaPlayer.pausePlayback();
                    break;
            }
        }
        return START_NOT_STICKY;
    }

    private void destroyMediaPlayer() {
        if (mMediaPlayer == null) {
            return;
        }
        mMediaPlayer.tearDown();
        mMediaPlayer = null;
    }

    /******************************
     * Service bind functions
     ******************************/

    @Override
    public boolean onUnbind(Intent intent) {

        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class AlarmServiceBinder extends Binder {
        public AlarmPlayerService getService() {
            return AlarmPlayerService.this;
        }
    }

    public static void triggerAlarm(Context context, String path) {
        PendingIntent pendingIntent = PendingIntent.getService(context, REQ_CODE_STOP_SERVICE,
                new Intent(context, AlarmPlayerService.class)
                        .setAction(AlarmPlayerService.ACTION_STOP_ALARM),
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        //setting up notification
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle("Alarm from FL Simple Player")
                .setContentText("Playing: " + path)
                .setSmallIcon(R.mipmap.ic_launcher)
                .addAction(R.drawable.ic_close, "Stop", pendingIntent)
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        nm.notify(0, notification);


        Intent intent = new Intent(context, AlarmPlayerService.class);
        intent.setAction(ACTION_PLAY_ALARM);
        intent.putExtra(PARAM_EPISODE_PATH, path);
        context.startService(intent);
    }

    public static void stopAlarm(Context context){
        Intent intent = new Intent(context, AlarmPlayerService.class);
        intent.setAction(ACTION_STOP_ALARM);
        context.startService(intent);
    }
}
