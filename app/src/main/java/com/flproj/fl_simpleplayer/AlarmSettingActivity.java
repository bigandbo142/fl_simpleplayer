package com.flproj.fl_simpleplayer;

import android.content.Intent;
import android.content.res.AssetManager;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.flproj.fl_simpleplayer.data.AppDatabase;
import com.flproj.fl_simpleplayer.helper.AlarmHelper;
import com.flproj.fl_simpleplayer.helper.FileHelper;
import com.flproj.fl_simpleplayer.pojos.AlarmSetting;
import com.flproj.fl_simpleplayer.widgets.TimePickerDialogFixedNougatSpinner;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.filter.entity.AudioFile;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;


import org.threeten.bp.LocalDateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.flproj.fl_simpleplayer.SimpleAudioPickerActivity.IS_NEED_DEFAULT;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_NEED_RECORDER;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_TAKEN_AUTO_SELECTED;

public class AlarmSettingActivity extends AppCompatActivity {

    private AlarmSettingAdapter mAdapter;
    private RecyclerView rv_alarms;
    private TextView tv_no_data;
    private FloatingActionButton fab_add;

    private AlarmSetting newAlarm = null;
    DialogProperties properties = new DialogProperties();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_setting);

        getViewReferences();
        initView();
        registerEvents();
    }

    private void getViewReferences(){
        rv_alarms = findViewById(R.id.rv_alarms);
        tv_no_data = findViewById(R.id.tv_no_data);
        fab_add = findViewById(R.id.fab_add);
    }

    private void initView(){
        copyFilesToSdCard();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions =  FileHelper.supportedAudioFormat;


        if(mAdapter == null){
            mAdapter = new AlarmSettingAdapter(this, new AlarmSettingAdapter.OnItemInteraction() {
                @Override
                public void onClickItem(AlarmSetting setting) {
                    mAdapter.updateItem(setting);
                }

                @Override
                public void onRepeatChange(AlarmSetting setting) {
                    Log.d("TEST888", "onRepeatChange " + setting.isRepeat());
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().update(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if(setting.isOn())
                                    AlarmHelper.addAlarm(setting, AlarmSettingActivity.this);
                                mAdapter.updateItem(setting);
                            }, throwable -> throwable.printStackTrace());
                }

                @Override
                public void onOnOffChange(AlarmSetting setting) {
                    Log.d("TEST888", "onOnOffChange " + setting.isOn());
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().update(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if(setting.isOn()){
                                    AlarmHelper.addAlarm(setting, AlarmSettingActivity.this);
                                }else{
                                    AlarmHelper.cancelAlarm(AlarmSettingActivity.this, setting);
                                }
                            }, throwable -> throwable.printStackTrace());


                }

                @Override
                public void onDeleteItem(AlarmSetting setting) {
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().delete(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if(aBoolean)
                                    if(setting.isOn()){
                                        AlarmHelper.cancelAlarm(AlarmSettingActivity.this, setting);
                                    }
                                    mAdapter.deleteItem(setting);
                            }, throwable -> throwable.printStackTrace());
                }

                @Override
                public void onChangeSchedule(AlarmSetting setting) {
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().update(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if(setting.isOn())
                                    AlarmHelper.addAlarm(setting, AlarmSettingActivity.this);
                                mAdapter.updateItem(setting);
                            }, throwable -> throwable.printStackTrace());
                }

                @Override
                public void onChangeSoundFile(AlarmSetting setting) {
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().update(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                mAdapter.updateItem(setting);
                            }, throwable -> throwable.printStackTrace());
                }

                @Override
                public void onChangeRepeatType(AlarmSetting setting) {
                    Single.fromCallable(() -> {
                        AppDatabase.getAppDatabase(getApplicationContext()).alarmSettingDao().update(setting);
                        return true;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if(setting.isOn())
                                    AlarmHelper.addAlarm(setting, AlarmSettingActivity.this);
                                mAdapter.updateItem(setting);
                            }, throwable -> throwable.printStackTrace());
                }
            });
        }

        rv_alarms.setLayoutManager(new LinearLayoutManager(this));
        rv_alarms.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rv_alarms.setAdapter(mAdapter);

        Single.fromCallable(() -> AppDatabase.getAppDatabase(this).alarmSettingDao().getAll())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(alarmSettings -> {
                    if(alarmSettings != null && alarmSettings.size() > 0){
                        tv_no_data.setVisibility(View.GONE);
                        rv_alarms.setVisibility(View.VISIBLE);
                        mAdapter.setData(alarmSettings);
                    }else{
                        rv_alarms.setVisibility(View.GONE);
                        tv_no_data.setVisibility(View.VISIBLE);
                    }
                }, throwable -> {
                    rv_alarms.setVisibility(View.GONE);
                    tv_no_data.setVisibility(View.VISIBLE);
                    throwable.printStackTrace();
                });


    }

    public final static String TARGET_BASE_PATH = "/sdcard/";

    private void copyFilesToSdCard() {

        if(!new File(TARGET_BASE_PATH + "clockchime.mp3").exists() || !new File(TARGET_BASE_PATH + "twotone.mp3").exists()) {
            copyFileOrDir("");
        }
    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            Log.i("tag", "copyFileOrDir() "+path);
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath =  TARGET_BASE_PATH + path;
                Log.i("tag", "path="+fullPath);
                File dir = new File(fullPath);
                if (!dir.exists() && !path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                    if (!dir.mkdirs())
                        Log.i("tag", "could not create dir "+fullPath);
                for (int i = 0; i < assets.length; ++i) {
                    String p;
                    if (path.equals(""))
                        p = "";
                    else
                        p = path + "/";

                    if (!path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                        copyFileOrDir( p + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        String newFileName = null;
        try {
            Log.i("tag", "copyFile() "+filename);
            in = assetManager.open(filename);
            if (filename.endsWith(".jpg")) // extension was added to avoid compression on APK file
                newFileName = TARGET_BASE_PATH + filename.substring(0, filename.length()-4);
            else
                newFileName = TARGET_BASE_PATH + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", "Exception in copyFile() of "+newFileName);
            Log.e("tag", "Exception in copyFile() "+e.toString());
        }

    }


    private void openAudioFilePicker(){
        Intent intent3 = new Intent(this, SimpleAudioPickerActivity.class);
        intent3.putExtra(IS_NEED_RECORDER, false);
        intent3.putExtra(IS_TAKEN_AUTO_SELECTED, false);
        intent3.putExtra(IS_NEED_DEFAULT, true);
        intent3.putExtra(Constant.MAX_NUMBER, 100);
        intent3.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent3, Constant.REQUEST_CODE_PICK_AUDIO);
    }

    private void registerEvents(){
        fab_add.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            newAlarm = new AlarmSetting();
                            newAlarm.setYear(year);
                            newAlarm.setMonth(monthOfYear);
                            newAlarm.setDay(dayOfMonth);

                            TimePickerDialogFixedNougatSpinner timePickerDialog = new TimePickerDialogFixedNougatSpinner(
                                    AlarmSettingActivity.this,
                                    new android.app.TimePickerDialog.OnTimeSetListener() {
                                        @Override
                                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {


                                            if(newAlarm != null) {

                                                Calendar cal = Calendar.getInstance();
                                                cal.set(Calendar.YEAR, newAlarm.getYear());
                                                cal.set(Calendar.MONTH, newAlarm.getMonth());
                                                cal.set(Calendar.DAY_OF_MONTH, newAlarm.getDay());
                                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                                cal.set(Calendar.MINUTE, minute);

                                                newAlarm.setHour(hourOfDay);
                                                newAlarm.setMinute(minute);
                                                newAlarm.setTriggerTime(String.valueOf(cal.getTimeInMillis()));


                                                openAudioFilePicker();

//                                                FilePickerDialog dialog = new FilePickerDialog(AlarmSettingActivity.this,properties);
//                                                dialog.setTitle("Select an audio");
//                                                dialog.setDialogSelectionListener(new DialogSelectionListener() {
//                                                    @Override
//                                                    public void onSelectedFilePaths(String[] files) {
//
//                                                        List<String> results = FileHelper.getAllAudioFileFromFilesOrDirectory(files);
//
//                                                        MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
//                                                        for (String path : results){
//                                                            metaRetriver.setDataSource(path);
//
//                                                            try{
//                                                                newAlarm.setSoundTitle(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
//                                                            }catch (Exception ex){
//                                                                newAlarm.setSoundTitle("Unknown");
//                                                            }
//
//                                                            newAlarm.setPath(path);
//                                                        }
//
//
//                                                        Single.fromCallable(() -> {
//                                                            try {
//                                                                AppDatabase.getAppDatabase(AlarmSettingActivity.this)
//                                                                        .alarmSettingDao()
//                                                                        .insertAll(newAlarm);
//
//                                                                return AppDatabase.getAppDatabase(AlarmSettingActivity.this)
//                                                                        .alarmSettingDao()
//                                                                        .getAll();
//                                                            }catch (Exception e){
//                                                                e.printStackTrace();
//                                                                return null;
//                                                            }
//                                                        })
//                                                                .subscribeOn(Schedulers.io())
//                                                                .observeOn(AndroidSchedulers.mainThread())
//                                                                .subscribe(settings -> {
//
//                                                                    if(settings != null && settings.size() > 0){
//                                                                        tv_no_data.setVisibility(View.GONE);
//                                                                        rv_alarms.setVisibility(View.VISIBLE);
//                                                                        mAdapter.setData(settings);
//                                                                    }else{
//                                                                        rv_alarms.setVisibility(View.GONE);
//                                                                        tv_no_data.setVisibility(View.VISIBLE);
//                                                                    }
//                                                                }, throwable -> {
//
//                                                                });
//
//                                                    }
//                                                });
//
//                                                dialog.show();
                                            }

                                        }
                                    },
                                    LocalDateTime.now().getHour(),
                                    LocalDateTime.now().getMinute(),
                                    false

                            );
                            timePickerDialog.setTitle("Select time for alarm");
                            timePickerDialog.show();
                        }
                    },
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
            dpd.setOnCancelListener(dialog -> {
                newAlarm = null;
            });
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Constant.REQUEST_CODE_PICK_AUDIO:
                if (resultCode == RESULT_OK) {

                    ArrayList<AudioFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_AUDIO);

                    List<String> results = new ArrayList<>();
                    for (AudioFile audioFile : list){
                        results.add(audioFile.getPath());
                    }

                    MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                    for (String path : results){
                        metaRetriver.setDataSource(path);

                        try{
                            newAlarm.setSoundTitle(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                        }catch (Exception ex){
                            newAlarm.setSoundTitle("Unknown");
                        }

                        newAlarm.setPath(path);
                    }


                    Single.fromCallable(() -> {
                        try {
                            AppDatabase.getAppDatabase(AlarmSettingActivity.this)
                                    .alarmSettingDao()
                                    .insertAll(newAlarm);

                            return AppDatabase.getAppDatabase(AlarmSettingActivity.this)
                                    .alarmSettingDao()
                                    .getAll();
                        }catch (Exception e){
                            e.printStackTrace();
                            return null;
                        }
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(settings -> {

                                if(settings != null && settings.size() > 0){
                                    tv_no_data.setVisibility(View.GONE);
                                    rv_alarms.setVisibility(View.VISIBLE);
                                    mAdapter.setData(settings);
                                }else{
                                    rv_alarms.setVisibility(View.GONE);
                                    tv_no_data.setVisibility(View.VISIBLE);
                                }
                            }, throwable -> {

                            });


                }
                break;
        }
    }
}
