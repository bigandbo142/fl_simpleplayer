package com.flproj.fl_simpleplayer.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.flproj.fl_simpleplayer.data.dao.AlarmSettingDao;
import com.flproj.fl_simpleplayer.data.dao.EpisodeDao;
import com.flproj.fl_simpleplayer.data.dao.PlaylistDao;
import com.flproj.fl_simpleplayer.pojos.AlarmSetting;
import com.flproj.fl_simpleplayer.pojos.Episode;
import com.flproj.fl_simpleplayer.pojos.Playlist;

/**
 * Created by hieunt on 3/15/18.
 */
@Database(entities = {
        Episode.class,
        Playlist.class,
        AlarmSetting.class
}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract PlaylistDao playlistDao();
    public abstract EpisodeDao episodeDao();
    public abstract AlarmSettingDao alarmSettingDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "player-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
