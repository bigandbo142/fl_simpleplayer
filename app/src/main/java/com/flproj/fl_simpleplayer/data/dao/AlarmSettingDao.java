package com.flproj.fl_simpleplayer.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.flproj.fl_simpleplayer.pojos.AlarmSetting;
import com.flproj.fl_simpleplayer.pojos.Episode;

import java.util.List;

/**
 * Created by hieunt on 3/15/18.
 */
@Dao
public interface AlarmSettingDao {
    @Query("SELECT * FROM settings ORDER BY uid DESC")
    List<AlarmSetting> getAll();

//    @Query("SELECT * FROM episode where playlist_id = :playlistId")
//    List<Episode> getAllByPlayListId(final int playlistId);
//
    @Query("SELECT * FROM settings where uid = :id")
    AlarmSetting findById(int id);

    @Query("SELECT COUNT(*) from settings")
    int countSettings();

    @Insert
    void insertAll(AlarmSetting... settings);

    @Delete
    void delete(AlarmSetting setting);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(AlarmSetting setting);
}
