package com.flproj.fl_simpleplayer.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.flproj.fl_simpleplayer.pojos.Episode;

import java.util.List;

/**
 * Created by hieunt on 3/15/18.
 */
@Dao
public interface EpisodeDao {
    @Query("SELECT * FROM episode")
    List<Episode> getAll();

    @Query("SELECT * FROM episode where playlist_id = :playlistId")
    List<Episode> getAllByPlayListId(final int playlistId);

    @Query("SELECT * FROM episode where uid = :id")
    Episode findById(int id);

    @Query("SELECT COUNT(*) from episode")
    int countEpisodes();

    @Insert
    void insertAll(Episode... episodes);

    @Delete
    void delete(Episode episode);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Episode episode);
}
