package com.flproj.fl_simpleplayer.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.flproj.fl_simpleplayer.pojos.Playlist;

/**
 * Created by hieunt on 3/15/18.
 */
@Dao
public interface PlaylistDao {
    @Insert
    long insertPlaylist(Playlist playlist);

    @Query("SELECT * FROM playlist where uid = :id")
    Playlist findById(int id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Playlist entity);

}
