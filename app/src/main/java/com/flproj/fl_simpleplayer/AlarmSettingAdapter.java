package com.flproj.fl_simpleplayer;

import android.content.Context;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.flproj.fl_simpleplayer.helper.DatetimeHelper;
import com.flproj.fl_simpleplayer.pojos.AlarmSetting;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hieunt on 3/16/18.
 */

public class AlarmSettingAdapter extends RecyclerView.Adapter<AlarmSettingAdapter.AlarmHolder>{

    private List<AlarmSetting> mData;
    private Context mContext;
    private OnItemInteraction mCallback;

    public interface OnItemInteraction {
        void onClickItem(AlarmSetting setting);
        void onDeleteItem(AlarmSetting setting);
        void onRepeatChange(AlarmSetting setting);
        void onOnOffChange(AlarmSetting setting);
        void onChangeSchedule(AlarmSetting setting);
        void onChangeSoundFile(AlarmSetting setting);
        void onChangeRepeatType(AlarmSetting setting);
    }

    public AlarmSettingAdapter(Context mContext, OnItemInteraction callback) {
        this.mContext = mContext;
        mData = new ArrayList<>();
        mCallback = callback;
    }

    @Override
    public AlarmHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_item_alarm_setting, parent, false);
        return new AlarmHolder(view);
    }

    @Override
    public void onBindViewHolder(AlarmHolder holder, int position) {
        holder.bindData(mData.get(position), mCallback);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<AlarmSetting> settings){
        mData.clear();
        mData.addAll(settings);
        notifyDataSetChanged();
    }

    public void deleteItem(AlarmSetting setting){
        if(mData != null && mData.size() > 0 && mData.contains(setting)){
            mData.remove(setting);
            notifyDataSetChanged();
        }
    }

    public void updateItem(AlarmSetting setting){
        for (int i = 0; i < mData.size(); i++){
            if(mData.get(i).getUid() == setting.getUid()){
                mData.set(i, setting);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public class AlarmHolder extends RecyclerView.ViewHolder {
        private View rootView;
        private ImageButton btn_delete;
        private TextView tv_file_name;
        private TextView tv_trigger_day;
        private TextView tv_trigger_time;
        private CheckBox cb_repeat;
        private Switch switch_on_off;
        private ConstraintLayout view_setting;
        private RadioGroup rd_group;
        private RadioButton rd_everyday;
        private RadioButton rd_first_tuesday;
        private RadioButton rd_third_tuesday;

        public AlarmHolder(View itemView) {
            super(itemView);
            rootView = itemView;

            cb_repeat = rootView.findViewById(R.id.cb_repeat);
            btn_delete = rootView.findViewById(R.id.btn_delete);
            switch_on_off = rootView.findViewById(R.id.switch_on_off);
            view_setting = rootView.findViewById(R.id.view_setting);
            rd_group = rootView.findViewById(R.id.rd_group);
            tv_file_name = rootView.findViewById(R.id.tv_file_name);
            tv_trigger_day = rootView.findViewById(R.id.tv_trigger_day);
            tv_trigger_time = rootView.findViewById(R.id.tv_trigger_time);
            rd_everyday = rootView.findViewById(R.id.rd_everyday);
            rd_first_tuesday = rootView.findViewById(R.id.rd_first_tuesday);
            rd_third_tuesday = rootView.findViewById(R.id.rd_third_tuesday);
        }

        public void bindData(AlarmSetting setting, OnItemInteraction callback){
            view_setting.setVisibility(setting.isShowSetting() ? View.VISIBLE : View.GONE);
            rd_group.setVisibility(setting.isRepeat() ? View.VISIBLE : View.GONE);
            rootView.setOnClickListener(v -> {
                setting.setShowSetting(!setting.isShowSetting());
                callback.onClickItem(setting);
            });
            cb_repeat.setChecked(setting.isRepeat());
            switch_on_off.setChecked(setting.isOn());
            cb_repeat.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setting.setRepeat(isChecked);
                callback.onRepeatChange(setting);
                rd_group.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            });

            switch (setting.getRepeatType()){
                case 0:
                    rd_everyday.setChecked(true);
                    rd_first_tuesday.setChecked(false);
                    rd_third_tuesday.setChecked(false);
                    break;
                case 1:
                    rd_everyday.setChecked(false);
                    rd_first_tuesday.setChecked(true);
                    rd_third_tuesday.setChecked(false);
                    break;
                case 2:
                    rd_everyday.setChecked(false);
                    rd_first_tuesday.setChecked(false);
                    rd_third_tuesday.setChecked(true);
                    break;
            }

            switch_on_off.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setting.setOn(isChecked);
                callback.onOnOffChange(setting);
            });

            tv_file_name.setText(setting.getSoundTitle());

            tv_trigger_day.setText(DatetimeHelper.convertToReadableDateFromTime(Long.valueOf(setting.getTriggerTime())));
            tv_trigger_time.setText(DatetimeHelper.convertTo24HoursFromTime(Long.valueOf(setting.getTriggerTime())));

            btn_delete.setOnClickListener(view -> callback.onDeleteItem(setting));


            rd_everyday.setOnCheckedChangeListener((compoundButton, checked) -> {
                if(checked){
                    setting.setRepeatType(0);
                    setting.setTriggerTime(String.valueOf(DatetimeHelper.getNextDayWithTime(setting.getHour(), setting.getMinute())));
                    rd_first_tuesday.setChecked(false);
                    rd_third_tuesday.setChecked(false);
                    callback.onChangeRepeatType(setting);
                }
            });

            rd_first_tuesday.setOnCheckedChangeListener((compoundButton, checked) -> {
                if(checked){
                    setting.setRepeatType(1);
                    setting.setTriggerTime(String.valueOf(DatetimeHelper.getNextFirstTuesdayWithTime(setting.getHour(), setting.getMinute())));
                    rd_everyday.setChecked(false);
                    rd_third_tuesday.setChecked(false);
                    callback.onChangeRepeatType(setting);
                }
            });

            rd_third_tuesday.setOnCheckedChangeListener((compoundButton, checked) -> {
                if(checked){
                    setting.setRepeatType(2);
                    setting.setTriggerTime(String.valueOf(DatetimeHelper.getNextThirdTuesdayWithTime(setting.getHour(), setting.getMinute())));
                    rd_everyday.setChecked(false);
                    rd_first_tuesday.setChecked(false);
                    callback.onChangeRepeatType(setting);
                }
            });

        }
    }

}
