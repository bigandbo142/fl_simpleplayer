package com.flproj.fl_simpleplayer.media;

/**
 * Created by hieunt on 3/14/18.
 */

public interface ProgressUpdateListener {

    void onProgressUpdate(long progress, long bufferedProgress, long duration);

}
