package com.flproj.fl_simpleplayer.media;

import com.flproj.fl_simpleplayer.pojos.Episode;

/**
 * Created by hieunt on 3/14/18.
 */

public abstract class MediaPlayer {

    protected SimpleMediaPlayerListener mMediaPlayerListener;
    protected ProgressUpdateListener mProgressUpdateListener;

    public MediaPlayer(SimpleMediaPlayerListener mediaPlayerListener,
                       ProgressUpdateListener progressUpdateListener) {
        mMediaPlayerListener = mediaPlayerListener;
        mProgressUpdateListener = progressUpdateListener;
    }

    public void tearDown() {
        mMediaPlayerListener = null;
        mProgressUpdateListener = null;
    }

    public abstract void loadEpisode(Episode episode);

    public abstract Episode getCurrentEpisode();

    public abstract void startPlayback(boolean playImmediately);

    public abstract void resumePlayback();

    public abstract void pausePlayback();

    public abstract void stopPlayback();

    public abstract void seekTo(long position);

    public abstract boolean isStreaming();

    public abstract int getState();

    public abstract long getCurrentPosition();

    public abstract long getDuration();

    public abstract long getBufferedPosition();

    public abstract void setPlaybackSpeed(float speed);

    public abstract float changeVolume(float volume);


    public interface SimpleMediaPlayerListener {

        void onStateChanged(int state);

    }

}
