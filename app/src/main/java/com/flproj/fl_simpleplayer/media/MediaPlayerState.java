package com.flproj.fl_simpleplayer.media;

/**
 * Created by hieunt on 3/14/18.
 */

public class MediaPlayerState {
    public static final int STATE_IDLE = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_PAUSED = 4;
    public static final int STATE_ENDED = 5;
}
